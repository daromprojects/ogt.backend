﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Ogt.Entities;
using Ogt.Entities.Responses;

namespace Ogt.Models
{
    public interface IStudentModel
    {
        Task<DeleteResponse> Delete(int idStudent);
        Task<GetResponse> Get(StudentsFilter filter);
        Task<UpdateResponse> Update(Student student);
    }
}