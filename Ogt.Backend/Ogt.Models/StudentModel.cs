﻿using Ogt.Entities;
using Ogt.Repository;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Ogt.Entities.Responses;

namespace Ogt.Models
{
    public class StudentModel : IStudentModel
    {
        private IStudentsRepository repository;
        private IValidate validate;

        public StudentModel(IStudentsRepository repository, IValidate validate)
        {
            this.repository = repository;
            this.validate = validate;
        }

        public async Task<UpdateResponse> Update(Student student)
        {
            UpdateResponse response = new UpdateResponse();
            int resultOperation = 0;

            if (validate.ValidateStudent(student))
            {
                resultOperation = await UpdateOperation(student, response, resultOperation);
            }
            else
            {
                response.Code = "100";
                response.Description = "Faltan datos del estudiante.";
            }

            response.Id = resultOperation;
            return response;
        }

        private async Task<int> UpdateOperation(Student student, UpdateResponse response, int resultOperation)
        {
            if (student.Id == 0)
            {
                var resultAll = await repository.GetAll();
                var IdMax = resultAll.Max(x => x.Id) + 1;
                student.Id = IdMax;
                resultOperation = await repository.Add(student);
            }
            else
            {
                resultOperation = await repository.Update(student);
            }
            response.Code = "100";
            response.Description = "Error en la operación.";
            if (resultOperation > 0)
            {
                response.Code = "1";
                response.Description = "Operación exitosa!";
            }

            return resultOperation;
        }

        public async Task<DeleteResponse> Delete(int idStudent)
        {
            DeleteResponse response = new DeleteResponse();
            bool resultDelete = false;
            if (idStudent != 0)
            {
                resultDelete = await repository.Delete(idStudent);
                response.Code = "1";
                response.Description = "Operación exitosa!";
            }
            if (resultDelete)
            {
                response.Code = "100";
                response.Description = "Error en la operación.";
            }
            response.Success = resultDelete;
            return response;
        }

        public async Task<GetResponse> Get(StudentsFilter filter)
        {
            GetResponse response = new GetResponse();
            List<Student> result = new List<Student>();
            result = await repository.GetAll();
            response.Code = "1";
            response.Description = "Operación exitosa!";
            if (filter.Id != 0 || !string.IsNullOrEmpty(filter.Search))
            {
                if (filter.Id == 0)
                {
                    result = (from student in result
                              where student.Career.Contains(filter.Search) ||
                                 student.FirstName.Contains(filter.Search) ||
                                 student.LastName.Contains(filter.Search) ||
                                 student.UserName.Contains(filter.Search)
                              select student).ToList();
                }
                else
                {
                    result = (from student in result
                              where student.Id == filter.Id
                              select student).ToList();
                }
            }
            response.Students = result;
            if (response.Students.Count == 0)
            {
                response.Code = "100";
                response.Description = "Error en la operación.";
            }
            return response;
        }


    }
}
