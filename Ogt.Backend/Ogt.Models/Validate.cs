﻿using System;
using System.Collections.Generic;
using System.Text;
using Ogt.Entities;

namespace Ogt.Models
{
    public class Validate : IValidate
    {
        public bool ValidateStudent(Student student)
        {
            bool result = false;
            result = !string.IsNullOrEmpty(student.UserName) && !string.IsNullOrEmpty(student.FirstName) 
                && !string.IsNullOrEmpty(student.LastName) && !(student.Age == 0) && 
                !string.IsNullOrEmpty(student.Career);
            return result;
        }
    }
}
