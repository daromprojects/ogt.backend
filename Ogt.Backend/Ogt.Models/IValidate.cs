﻿using Ogt.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ogt.Models
{
    public interface IValidate
    {
        bool ValidateStudent(Student student);
    }
}
