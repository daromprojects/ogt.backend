﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Ogt.Models;
using Ogt.Entities;
using Newtonsoft.Json;

namespace Ogt.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {

        private IStudentModel studentModel;

        public StudentsController(IStudentModel studentModel)
        {
            this.studentModel = studentModel;
        }

        // GET api/students
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var result = await studentModel.Get(new StudentsFilter());
            return new OkObjectResult(result);
        }

        // GET api/students/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var result = await studentModel.Get(new StudentsFilter() { Id = id, Search = null });
            return new OkObjectResult(result);
        }

        // POST api/students
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Student value)
        {
            //dynamic data = JsonConvert.DeserializeObject<Student>(value);
            var result = await studentModel.Update(value);
            return new OkObjectResult(result);
        }


        // DELETE api/students
        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await studentModel.Delete(id);
            return new OkObjectResult(result);
        }
    }
}
