﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ogt.Entities
{
    public class StudentsFilter
    {
        public int Id { get; set; }
        public string Search { get; set; }
    }
}
