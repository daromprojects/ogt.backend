﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ogt.Entities.Responses
{
    public class UpdateResponse : GenericReponse
    {
        public int Id { get; set; }
    }
}
