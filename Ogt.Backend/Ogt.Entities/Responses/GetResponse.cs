﻿using System;
using System.Collections.Generic;
using System.Text;
using Ogt.Entities;

namespace Ogt.Entities.Responses
{
    public class GetResponse : GenericReponse
    {
        public List<Student> Students { get; set; }
    }
}
