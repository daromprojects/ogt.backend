﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ogt.Entities.Responses
{
    public class GenericReponse
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
