﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ogt.Entities.Responses
{
    public class DeleteResponse : GenericReponse
    {
        public bool Success { get; set; }
    }
}
