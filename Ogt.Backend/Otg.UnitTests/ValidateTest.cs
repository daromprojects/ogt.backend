using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ogt.Models;
using Ogt.Entities;
using Ogt.Repository;

namespace Otg.UnitTests
{
    [TestClass]
    public class ValidateTest
    {
        
        [TestMethod]
        public void ValidateMethodExistsAndReturnsSomething()
        {
            Validate validate = new Validate();
            Student student = new Student();
            var result = validate.ValidateStudent(student);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ValidateMethodReturnsFalseWhenStudentsUserNameItsEmpty()
        {
            Validate validate = new Validate();
            Student student = new Student()
            {
                UserName = string.Empty,
                FirstName = "FName",
                LastName = "LName",
                Age = 100,
                Career = "CName"
            };
            var result = validate.ValidateStudent(student);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ValidateMethodReturnsFalseWhenStudentsFirstNameItsEmpty()
        {
            Validate validate = new Validate();
            Student student = new Student()
            {
                UserName = "UName",
                FirstName = string.Empty,
                LastName = "LName",
                Age = 100,
                Career = "CName"
            };
            var result = validate.ValidateStudent(student);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ValidateMethodReturnsFalseWhenStudentsLastNameItsEmpty()
        {
            Validate validate = new Validate();
            Student student = new Student()
            {
                UserName = "UName",
                FirstName = "FName",
                LastName = string.Empty,
                Age = 100,
                Career = "CName"
            };
            var result = validate.ValidateStudent(student);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ValidateMethodReturnsFalseWhenStudentsAgeIts0()
        {
            Validate validate = new Validate();
            Student student = new Student()
            {
                UserName = "UName",
                FirstName = "FName",
                LastName = "LName",
                Age = 0,
                Career = "CName"
            };
            var result = validate.ValidateStudent(student);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ValidateMethodReturnsFalseWhenStudentsCarerItsEmpty()
        {
            Validate validate = new Validate();
            Student student = new Student()
            {
                UserName = "UName",
                FirstName = "FName",
                LastName = "LName",
                Age = 100,
                Career = string.Empty
            };
            var result = validate.ValidateStudent(student);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void ValidateMethodReturnsTrueWhenStudentsFieldsAreFilled()
        {
            Validate validate = new Validate();
            Student student = new Student()
            {
                UserName = "UName",
                FirstName = "FName",
                LastName = "LName",
                Age = 100,
                Career = "CName"
            };
            var result = validate.ValidateStudent(student);
            Assert.IsTrue(result);
        }

    }
}
