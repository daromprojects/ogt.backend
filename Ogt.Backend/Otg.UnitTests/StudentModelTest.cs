using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ogt.Models;
using Ogt.Entities;
using Ogt.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otg.UnitTests
{
    [TestClass]
    public class StudentModelTest

    {
        Mock<IStudentsRepository> mockRepository;
        Mock<IValidate> mockValidate;
        private IStudentsRepository repository;
        private IValidate validate;

        [TestMethod]
        public void UpdateMethodExistsAndReturnSomething()
        {
            mockRepository = new Mock<IStudentsRepository>();
            mockValidate = new Mock<IValidate>();
            repository = mockRepository.Object;
            validate = mockValidate.Object;
            StudentModel model = new StudentModel(repository, validate);
            Student student = new Student();
            var result = model.Update(student);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void UpdateMethodReturns0WhenRepoCantDoIt()
        {
            mockRepository = new Mock<IStudentsRepository>();
            mockValidate = new Mock<IValidate>();
            List<Student> students = new List<Student>();
            students.Add(new Student() { Id = 1, UserName = "UNameString", FirstName = "FName", LastName = "LName", Career = "Carer" });
            students.Add(new Student() { Id = 2, UserName = "UNameString", FirstName = "FNameString", LastName = "LNameString", Career = "CarerString" });
            students.Add(new Student() { Id = 3, UserName = "UName", FirstName = "FName", LastName = "LName", Career = "Carer" });
            students.Add(new Student() { Id = 4, UserName = "UName", FirstName = "FName", LastName = "LNameString", Career = "Carer" });
            Student student = new Student();
            mockValidate.Setup(it => it.ValidateStudent(student)).Returns(true);
            validate = mockValidate.Object;
            mockRepository.Setup(it => it.Add(student)).Returns(Task.FromResult(0));
            mockRepository.Setup(it => it.GetAll()).Returns(Task.FromResult(students));
            repository = mockRepository.Object;
            StudentModel model = new StudentModel(repository, validate);
            var result = model.Update(student).GetAwaiter().GetResult();
            Assert.IsTrue(result.Id == 0);
        }

        [TestMethod]
        public void UpdateMethodReturns0WhenStudentItsNotValid()
        {
            mockRepository = new Mock<IStudentsRepository>();
            mockValidate = new Mock<IValidate>();
            Student student = new Student();
            mockValidate.Setup(it => it.ValidateStudent(student)).Returns(false);
            validate = mockValidate.Object;
            repository = mockRepository.Object;
            StudentModel model = new StudentModel(repository, validate);
            var result = model.Update(student).GetAwaiter().GetResult();
            Assert.IsTrue(result.Id == 0);
        }

        [TestMethod]
        public void UpdateMethodUsesRepoMethodAddWhenStudentIdIts0()
        {
            mockRepository = new Mock<IStudentsRepository>();
            mockValidate = new Mock<IValidate>();
            List<Student> students = new List<Student>();
            students.Add(new Student() { Id = 1, UserName = "UNameString", FirstName = "FName", LastName = "LName", Career = "Carer" });
            students.Add(new Student() { Id = 2, UserName = "UNameString", FirstName = "FNameString", LastName = "LNameString", Career = "CarerString" });
            students.Add(new Student() { Id = 3, UserName = "UName", FirstName = "FName", LastName = "LName", Career = "Carer" });
            students.Add(new Student() { Id = 4, UserName = "UName", FirstName = "FName", LastName = "LNameString", Career = "Carer" });
            Student student = new Student() { Id = 0 };
            mockValidate.Setup(it => it.ValidateStudent(student)).Returns(true);
            validate = mockValidate.Object;
            mockRepository.Setup(it => it.Add(student)).Returns(Task.FromResult(1));
            mockRepository.Setup(it => it.GetAll()).Returns(Task.FromResult(students));
            repository = mockRepository.Object;
            StudentModel model = new StudentModel(repository, validate);
            var result = model.Update(student).GetAwaiter().GetResult();
            mockRepository.VerifyAll();
        }

        [TestMethod]
        public void UpdateMethodUsesRepoMethodUpdateWhenStudentIdIts1()
        {
            mockRepository = new Mock<IStudentsRepository>();
            mockValidate = new Mock<IValidate>();
            Student student = new Student() { Id = 1 };
            mockValidate.Setup(it => it.ValidateStudent(student)).Returns(true);
            validate = mockValidate.Object;
            mockRepository.Setup(it => it.Update(student)).Returns(Task.FromResult(1));
            repository = mockRepository.Object;
            StudentModel model = new StudentModel(repository, validate);
            var result = model.Update(student).GetAwaiter().GetResult();
            mockRepository.VerifyAll();
        }

        [TestMethod]
        public void DeleteMethodExistAndReturnsSomething()
        {
            mockRepository = new Mock<IStudentsRepository>();
            mockValidate = new Mock<IValidate>();
            int idStudent = 0;
            repository = mockRepository.Object;
            validate = mockValidate.Object;
            StudentModel model = new StudentModel(repository, validate);
            var result = model.Delete(idStudent).GetAwaiter().GetResult();
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void DeleteMethodReturnsFalseIfIdStudentIts0()
        {
            mockRepository = new Mock<IStudentsRepository>();
            mockValidate = new Mock<IValidate>();
            int idStudent = 0;
            repository = mockRepository.Object;
            validate = mockValidate.Object;
            StudentModel model = new StudentModel(repository, validate);
            var result = model.Delete(idStudent).GetAwaiter().GetResult();
            Assert.IsFalse(result.Success);
        }

        [TestMethod]
        public void DeleteMethodExecuteMethodDeleteIfIdStudentItsHigherThan0()
        {
            mockRepository = new Mock<IStudentsRepository>();
            int idStudent = 1;
            mockRepository.Setup(it => it.Delete(idStudent)).Returns(Task.FromResult(true));
            mockValidate = new Mock<IValidate>();
            repository = mockRepository.Object;
            validate = mockValidate.Object;
            StudentModel model = new StudentModel(repository, validate);
            var result = model.Delete(idStudent).GetAwaiter().GetResult();
            mockRepository.VerifyAll();
        }

        [TestMethod]
        public void DeleteMethodReturnsTrueIfIdStudentItsHigherThan0()
        {
            mockRepository = new Mock<IStudentsRepository>();
            mockValidate = new Mock<IValidate>();
            int idStudent = 1;
            mockRepository.Setup(it => it.Delete(idStudent)).Returns(Task.FromResult(true));
            repository = mockRepository.Object;
            validate = mockValidate.Object;
            StudentModel model = new StudentModel(repository, validate);
            var result = model.Delete(idStudent).GetAwaiter().GetResult();
            Assert.IsTrue(result.Success);
        }

        [TestMethod]
        public void GetMethodExistsAndReturnSomething()
        {
            mockRepository = new Mock<IStudentsRepository>();
            List<Student> students = new List<Student>();
            mockRepository.Setup(it => it.GetAll()).Returns(Task.FromResult(students));
            mockValidate = new Mock<IValidate>();
            StudentsFilter filter = new StudentsFilter();
            repository = mockRepository.Object;
            validate = mockValidate.Object;
            StudentModel model = new StudentModel(repository, validate);
            var result = model.Get(filter).GetAwaiter().GetResult();
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetMethodReturnsAllIfFilterItsEmpty()
        {
            mockRepository = new Mock<IStudentsRepository>();
            List<Student> students = new List<Student>();
            students.Add(new Student());
            students.Add(new Student());
            students.Add(new Student());
            mockRepository.Setup(it => it.GetAll()).Returns(Task.FromResult(students));
            mockValidate = new Mock<IValidate>();
            StudentsFilter filter = new StudentsFilter();
            repository = mockRepository.Object;
            validate = mockValidate.Object;
            StudentModel model = new StudentModel(repository, validate);
            var result = model.Get(filter).GetAwaiter().GetResult();
            Assert.IsTrue(result.Students.Count == students.Count);
        }

        [TestMethod]
        public void GetMethodReturns3RegsOftheDataWithTheFilterHasASearchString()
        {
            mockRepository = new Mock<IStudentsRepository>();
            List<Student> students = new List<Student>();
            students.Add(new Student() { Id = 1, UserName = "UNameString", FirstName = "FName", LastName = "LName", Career = "Carer" });
            students.Add(new Student() { Id = 2, UserName = "UNameString", FirstName = "FNameString", LastName = "LNameString", Career = "CarerString" });
            students.Add(new Student() { Id = 3, UserName = "UName", FirstName = "FName", LastName = "LName", Career = "Carer" });
            students.Add(new Student() { Id = 4, UserName = "UName", FirstName = "FName", LastName = "LNameString", Career = "Carer" });
            mockRepository.Setup(it => it.GetAll()).Returns(Task.FromResult(students));
            mockValidate = new Mock<IValidate>();
            StudentsFilter filter = new StudentsFilter() { Id = 0, Search = "String" };
            repository = mockRepository.Object;
            validate = mockValidate.Object;
            StudentModel model = new StudentModel(repository, validate);
            var result = model.Get(filter).GetAwaiter().GetResult();
            Assert.IsTrue(result.Students.Count == 3);
        }

        [TestMethod]
        public void GetMethodReturns1RegOftheDataWithTheFilterHasAId()
        {
            mockRepository = new Mock<IStudentsRepository>();
            List<Student> students = new List<Student>();
            students.Add(new Student() { Id = 1, UserName = "UNameString", FirstName = "FName", LastName = "LName", Career = "Carer" });
            students.Add(new Student() { Id = 2, UserName = "UNameString", FirstName = "FNameString", LastName = "LNameString", Career = "CarerString" });
            students.Add(new Student() { Id = 3, UserName = "UName", FirstName = "FName", LastName = "LName", Career = "Carer" });
            students.Add(new Student() { Id = 4, UserName = "UName", FirstName = "FName", LastName = "LNameString", Career = "Carer" });
            mockRepository.Setup(it => it.GetAll()).Returns(Task.FromResult(students));
            mockValidate = new Mock<IValidate>();
            StudentsFilter filter = new StudentsFilter() { Id = 2, Search = "String" };
            repository = mockRepository.Object;
            validate = mockValidate.Object;
            StudentModel model = new StudentModel(repository, validate);
            var result = model.Get(filter).GetAwaiter().GetResult();
            Assert.IsTrue(result.Students.Count == 1);
        }

        [TestMethod]
        public void GetMethodReturnsAnEmptyListIfDataItsEmpty()
        {
            mockRepository = new Mock<IStudentsRepository>();
            List<Student> students = new List<Student>();
            mockRepository.Setup(it => it.GetAll()).Returns(Task.FromResult(students));
            mockValidate = new Mock<IValidate>();
            StudentsFilter filter = new StudentsFilter();
            repository = mockRepository.Object;
            validate = mockValidate.Object;
            StudentModel model = new StudentModel(repository, validate);
            var result = model.Get(filter).GetAwaiter().GetResult();
            Assert.IsTrue(result.Students.Count == 0);
        }

    }
}
