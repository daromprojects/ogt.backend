﻿using System;
using System.Collections.Generic;
using System.Text;
using Ogt.Entities;
using System.Data.SQLite;
using System.Threading.Tasks;

namespace Ogt.Repository
{
    public class DatabaseService : IStudentsRepository
    {
        private string connectionString = @"URI=file:D:/SourceCode/ogt/ogt.backend/Ogt.Backend/Database/sample.db3";

        public async Task<int> Add(Student student)
        {
            int result = 0;
            using (SQLiteConnection connection = new SQLiteConnection(connectionString))
            {
                connection.Open();
                using (SQLiteCommand command = new SQLiteCommand("Insert into student values (@Id, @UserName, @FirstName, @LastName, @Age, @Career)", connection))
                {
                    command.Parameters.Add(new SQLiteParameter("@Id", student.Id));
                    command.Parameters.Add(new SQLiteParameter("@UserName", student.UserName));
                    command.Parameters.Add(new SQLiteParameter("@FirstName", student.FirstName));
                    command.Parameters.Add(new SQLiteParameter("@LastName", student.LastName));
                    command.Parameters.Add(new SQLiteParameter("@Age", student.Age));
                    command.Parameters.Add(new SQLiteParameter("@Career", student.Career));
                    var resultCommand = await command.ExecuteNonQueryAsync();
                    result = student.Id;
                }
            }
            return result;
        }

        public async Task<bool> Delete(int id)
        {
            bool result = true;
            try
            {
                using (SQLiteConnection connection = new SQLiteConnection(connectionString))
                {
                    connection.Open();
                    using (SQLiteCommand command = new SQLiteCommand("delete from student where id=@id", connection))
                    {
                        command.Parameters.Add(new SQLiteParameter("@id", id));
                        var resultCommand = await command.ExecuteNonQueryAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public async Task<List<Student>> GetAll()
        {
            List<Student> result = new List<Student>();
            using (SQLiteConnection connection = new SQLiteConnection(connectionString))
            {
                connection.Open();
                using (SQLiteCommand command = new SQLiteCommand("select * from student order by UserName", connection))
                {
                    var resultCommand = await command.ExecuteReaderAsync();
                    while (resultCommand.Read())
                    {
                        result.Add(new Student()
                        {
                            Id = resultCommand.GetInt16(0),
                            UserName = resultCommand.GetString(1),
                            FirstName = resultCommand.GetString(2),
                            LastName = resultCommand.GetString(3),
                            Age = resultCommand.GetInt16(4),
                            Career = resultCommand.GetString(5)
                        });
                    }
                }
            }
            return result;
        }

        public async Task<int> Update(Student student)
        {
            int result = 0;
            using (SQLiteConnection connection = new SQLiteConnection(connectionString))
            {
                connection.Open();
                using (SQLiteCommand command = new SQLiteCommand("Update student  set UserName = @UserName, " +
                    "FirstName = @FirstName, LastName = @LastName, Age = @Age, Career = @Career where Id = @Id", connection))
                {
                    command.Parameters.Add(new SQLiteParameter("@Id", student.Id));
                    command.Parameters.Add(new SQLiteParameter("@UserName", student.UserName));
                    command.Parameters.Add(new SQLiteParameter("@FirstName", student.FirstName));
                    command.Parameters.Add(new SQLiteParameter("@LastName", student.LastName));
                    command.Parameters.Add(new SQLiteParameter("@Age", student.Age));
                    command.Parameters.Add(new SQLiteParameter("@Career", student.Career));
                    var resultCommand = await command.ExecuteNonQueryAsync();
                    result = student.Id;
                }
            }
            return result;
        }
    }
}
