﻿using Ogt.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ogt.Repository
{
    public interface IStudentsRepository
    {
        Task<int> Add(Student student);
        Task<bool> Delete(int id);
        Task<int> Update(Student student);
        Task<List<Student>> GetAll();
    }
}
